package com.example.mira.briskula;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;

import com.example.mira.briskula.UCT;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Mira on 19.2.2016..
 */
public class MCBriskula {
    public Integer players;
    public List<Integer> karte = new ArrayList<Integer>();      //alokaciju premjestit u konstruktor!
    public List<Integer> izasle = new ArrayList<Integer>();
    public Map<Integer, List<Integer>> karte_igraca = new HashMap<Integer, List<Integer>>();
    public List<Integer> karte_za_bacanje = new ArrayList<Integer>();
    public Integer pobjednik;
    public List<Integer> bodovi = new ArrayList<Integer>();
    public Integer player_na_potezu;
    public Integer broj_karti_na_stolu;
    public Integer iteracije;
    public Integer briskula;
    public Integer broj_karti;

    MCBriskula(Integer broj_iteracija, Integer broj_igraca,Integer broj_karti_tmp) {
        for (Integer i = 0; i < 40; ++i)
            karte.add(i);
        players = broj_igraca;
        for (Integer i = 0; i < players; ++i)
            karte_za_bacanje.add(-1);
        pobjednik = 1;
        for (Integer i = 0; i < 2; ++i)
            this.bodovi.add(0);
        for (Integer i = 0; i < players; ++i) {
            List<Integer> nova = new ArrayList<Integer>();
            karte_igraca.put(i, nova);
        }
        player_na_potezu = 2;
        broj_karti_na_stolu = 0;
        iteracije = broj_iteracija;
        broj_karti=broj_karti_tmp;
    }

    public int igra_comp() {
        UCT novi=new UCT();
        int karta_za_bacanje = novi.izracunaj(this);
        DoMove(karta_za_bacanje);
        return karta_za_bacanje;
    }

    public void odigrao(Integer karta){
        DoMove(karta);
    }

    MCBriskula Clone() {
        MCBriskula nova = new MCBriskula(iteracije, players, broj_karti);
        nova.briskula = briskula;
        nova.izasle.clear();
        nova.izasle.addAll(izasle);
        nova.karte.clear();
        nova.karte.addAll(karte);
        nova.karte_za_bacanje.clear();
        nova.karte_za_bacanje.addAll(karte_za_bacanje);
        nova.karte_igraca.clear();
        for(Integer i=0; i<players; ++i){
            List<Integer> nova_l = new ArrayList<Integer>();
            for(Integer j=0; j<karte_igraca.get(i).size(); ++j)
                nova_l.add(karte_igraca.get(i).get(j));
            nova.karte_igraca.put(i,nova_l);
        }
        nova.bodovi.clear();
        nova.bodovi.addAll(bodovi);
        nova.player_na_potezu = player_na_potezu;
        nova.pobjednik = pobjednik;
        nova.broj_karti_na_stolu = broj_karti_na_stolu;
        nova.broj_karti=broj_karti;
        return nova;
    }

    void baci_kartu(int move) {
        //Log.d("briskula", "BACI KARTU()");
        karte_za_bacanje.set(player_na_potezu - 1, move);
        izasle.add(karte_igraca.get(player_na_potezu - 1).get(move));
        List<Integer> tmp = karte_igraca.get(player_na_potezu - 1);
        tmp.remove(move);
        karte_igraca.put(player_na_potezu - 1, tmp);
        broj_karti_na_stolu += 1;

    }

    void update_pobjendnika_i_bodova(){
        //Log.d("briskula", "UPDATE_POBJENDIKA_I_BODOVA()");
        List<Integer> lista = trenutno_uzima(players);
        //Log.d("briskula", "ispis liste"+Integer.toString(lista.get(0))+Integer.toString(lista.get(1)));
        Integer tko=lista.get(1)%2;
        Integer koliko = lista.get(0);
        Integer tmp = bodovi.get(tko);
        //Log.d("briskula", "UPDATE_POBJENDIKA_I_BODOVA() TKO I KOLIKO"+Integer.toString(tko)+Integer.toString(koliko));
        tmp+=koliko;
        bodovi.set(tko, tmp);
        pobjednik = lista.get(1);
        //Log.d("briskula", "UPDATE_POBJENDIKA_I_BODOVA POSTAVIO BODOVE()");
        player_na_potezu=lista.get(1)+1;
        //Log.d("briskula", "UPDATE_POBJENDIKA_I_BODOVA KRAAAJ()");
    }

    void podjeli_karte(){
        //Log.d("briskula", "PODJELI_KARTE()");
        for(int i=player_na_potezu-1; i<players+player_na_potezu-1; ++i){
            List<Integer> tmp = karte_igraca.get(i%players);
            tmp.add(dodjeli_kartu());
            karte_igraca.put(i%players, tmp);
        }
    }

    void podjeli_karte_zadnji_put(){
        //Log.d("briskula", "PODJELI_KARTE_ZADNJI_PUT()");
        for(int i=player_na_potezu-1; i<players+player_na_potezu-2; ++i){
            List<Integer> tmp = karte_igraca.get(i%players);
            tmp.add(dodjeli_kartu());
            karte_igraca.put(i%players, tmp);
        }
        List<Integer> tmp = karte_igraca.get(player_na_potezu%players);
        tmp.add(briskula);
        karte_igraca.put(player_na_potezu%players, tmp);
    }
    public void DoMove(int move){
        //Log.d("briskula", "U DOMOVE()");
        baci_kartu(move);
        if(broj_karti_na_stolu==players){
            update_pobjendnika_i_bodova();
            if(karte.size()==players-1)
                podjeli_karte_zadnji_put();
            if(karte.size()>players)
                podjeli_karte();
            broj_karti_na_stolu=0;
        }
        else{
            player_na_potezu+=1;
            if(player_na_potezu>players)
                player_na_potezu-=players;
        }
    }
    List<Integer> GetMoves(){
       // Log.d("briskula", "U GETmOVES()");
        List<Integer> lista = new ArrayList<Integer>();
        List<Integer> tmp = karte_igraca.get(player_na_potezu-1);
        for(int i=0; i<tmp.size(); ++i)
            lista.add(i);
        return lista;
    }

    Double GetResult(Integer player_tmp){
        //Log.d("briskula", "U GETrESULT()");
        if(player_tmp == 2 && bodovi.get(1)>bodovi.get(0))
            return 1.0;
        else if(player_tmp==1 && bodovi.get(0)>bodovi.get(1))
            return 1.0;
        else if(bodovi.get(0)==bodovi.get(1))
            return 0.5;
        else
            return 0.0;
    }

    Integer dodjeli_kartu(){
        //Log.d("briskula", "U DODJELI_KARTU()");
        Random randomGenerator = new Random();
        Integer index= randomGenerator.nextInt(karte.size());
        Integer vracam = karte.get(index);
        karte.remove(Integer.valueOf(vracam));
        //Log.d("briskula", "duljina karte"+Integer.toString(this.karte.size()));
        return vracam;
    }

    void podjeli_karte_na_pocetku(Integer igrac){
        for(int i=0; i<broj_karti; ++i)
            karte_igraca.get(igrac).add(dodjeli_kartu());
    }
    void postavi_briskulu(){
        briskula=dodjeli_kartu();
    }

    Integer poeni(Integer karta){
        //Log.d("briskula", "U POENI()");
        if(karta%10==0 || karta%10==1 || karta%10==2 || karta%10 ==3 ||karta%10 ==4)
            return 0;
        else if(karta%10 ==5 || karta%10 ==6 || karta%10 ==7)
            return karta%10-3;
        else if(karta%10 ==8)
            return 10;
        else
            return 11;
    }

    Boolean je_li_briskula(Integer karta){
        if(karta/10 == briskula/10)
            return Boolean.TRUE;
        else
            return Boolean.FALSE;
    }

    void tko_je_pobjedio(){
        List<Integer> tmp = trenutno_uzima(players);
        pobjednik = tmp.get(1);
        Integer bod = bodovi.get(pobjednik%2);
        bod+=tmp.get(0);
       bodovi.set(pobjednik%2, bod);
    }

    List<Integer> trenutno_uzima(Integer broj_karti_tmp){
        //Log.d("briskula", "U TRENUTNO_UZIMA()");
        List<Integer> tmp = new ArrayList<Integer>();
        for(Integer i=0; i<3; ++i)
            tmp.add(0);
        Integer najveca_briskula = -1;
        Integer najveca_prva_boja = -1;
        Integer poeni_u_krugu = 0;
        Integer nova_karta, prva_karta=0;
        for(Integer i=0; i<broj_karti_tmp; ++i){
            nova_karta = izasle.get(izasle.size() - broj_karti_tmp + i);
            if(i==0)
                prva_karta=nova_karta;
            poeni_u_krugu+=poeni(nova_karta);
            if(je_li_briskula(nova_karta) && nova_karta>najveca_briskula)
                najveca_briskula=nova_karta;
            if(nova_karta/10 == prva_karta/10 && nova_karta>=prva_karta)
                najveca_prva_boja=nova_karta;
        }
        if(najveca_briskula!=-1){
            if(izasle.get(izasle.size()-2)==najveca_briskula){
                tmp.set(0, poeni_u_krugu);
                tmp.set(1, pobjednik);
                tmp.set(2, najveca_briskula);
                return tmp;
            }
            else{
                tmp.set(0, poeni_u_krugu);
                tmp.set(1, (pobjednik+1)%2);
                tmp.set(2, najveca_briskula);
                return tmp;
            }
        }
        else{
            if(izasle.get(izasle.size()-2)==najveca_prva_boja){
                tmp.set(0, poeni_u_krugu);
                tmp.set(1, pobjednik);
                tmp.set(2, najveca_prva_boja);
            }
            else{
                tmp.set(0, poeni_u_krugu);
                tmp.set(1, (pobjednik+1)%2);
                tmp.set(2, najveca_prva_boja);
            }
            return tmp;
        }
    }
    public void print(){
        String str1="", str2="";
        for (Integer i = 0; i < karte_igraca.get(0).size(); ++i) {
            str1=str1.concat(Integer.toString(karte_igraca.get(0).get(i)));
            str1=str1.concat("; ");
        }
        for (Integer i = 0; i < karte_igraca.get(1).size(); ++i) {
            str2=str2.concat(Integer.toString(karte_igraca.get(1).get(i)));
            str2=str2.concat("; ");
        }
    }

}

