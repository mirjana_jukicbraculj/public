package com.example.mira.briskula;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mira on 20.2.2016..
 */
public class Node {
    Integer move;
    Node parentNode;
    List<Node> childNodes = new ArrayList<Node>();
    Double wins;
    Double visits;
    List<Integer> untriesMoves = new ArrayList<Integer>();
    Integer player_na_potezu;
    List<Integer> bodovi = new ArrayList<Integer>();
    Integer prvi_igrac;

    Node(){
        this.move = -1;
    }
    Node(Integer move, Node parent, MCBriskula state){
        this.move = move;
        this.parentNode = parent;
        this.wins = 0.0;
        this.visits = 0.0;
        this.untriesMoves = state.GetMoves();
        this.player_na_potezu = state.player_na_potezu;
        this.bodovi.addAll(state.bodovi);
        this.prvi_igrac = state.player_na_potezu;
    }
    Node UCTSelectChild(){
        //Log.d("briskula", "u uctSELECT CHILD");
        Node maxNode = new Node();
        Node minNode = new Node();
        Double mini=1.1;
        Double maxi = -1.0;

        for(int i=0; i<this.childNodes.size(); ++i){
            Double tmp = this.childNodes.get(i).wins/this.childNodes.get(i).visits+Math.sqrt(2*Math.log(this.visits)/this.childNodes.get(i).visits);
            if(maxi < tmp){
                maxNode = this.childNodes.get(i);
                maxi = tmp;
            }
        }
        //Log.d("briskula", Double.toString(maxi)+"izabrano dijete");
        return maxNode;
    }
    Node AddChild(Integer m, MCBriskula s){
        Node n = new Node(m, this, s);
        this.untriesMoves.remove(m);
        this.childNodes.add(n);
        return n;
    }
    void Update(Double result){
        this.visits+=1;
        this.wins+=result;
    }
    void print(){
        Log.d("briskula", "vins/visits"+Double.toString(this.wins)+"/"+Double.toString(this.visits));
    }
    void ispis_stabla(){
        Node pocetak = this;
        while (pocetak.childNodes.size()!=0){
            for(int i=0; i<childNodes.size();++i)
                childNodes.get(i).print();
            for(int i=0; i<childNodes.size();++i){
                pocetak = childNodes.get(i);
                pocetak.ispis_stabla();
            }
        }
    }


}
