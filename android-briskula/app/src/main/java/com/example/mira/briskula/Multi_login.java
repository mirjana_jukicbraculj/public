package com.example.mira.briskula;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mira on 03.04.16..
 */
public class Multi_login extends Activity {

    @Override
    protected void onCreate( Bundle savedInstanceState ){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multiplayer_login);

        TextView zaboravio = (TextView)findViewById(R.id.zaboravih);
        TextView bez_racuna = (TextView)findViewById(R.id.bez_racuna);
        TextView register = (TextView)findViewById(R.id.register);

        zaboravio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent("com.example.mira.Multi_obnovaLozinke");
                startActivity(i);
            }
        });
        bez_racuna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.Multi_wait");
                startActivity(i);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.Multi_register");
                startActivity(i);
            }
        });
    }

    ///funkcija se spaja sa serverom saljuci mu username i pass preko POSTRequest-a
    public void logIn( View view ){

        //idemo u novu activity
        Intent i = new Intent("com.example.mira.Multi_wait");
        EditText user = (EditText)findViewById(R.id.username);
        EditText pass = (EditText)findViewById(R.id.pass);
        i.putExtra( "username", user.getText().toString());
        i.putExtra("pass", pass.getText().toString());
        Log.d("briskula", "sve spremno");
        startActivity(i);
        //new Thread( new spajanje() ).start();
    }
    @Override
    public void onBackPressed()
    {
        Intent i = new Intent("com.examle.mira.MainActivity");
        startActivity(i);

    }
}


