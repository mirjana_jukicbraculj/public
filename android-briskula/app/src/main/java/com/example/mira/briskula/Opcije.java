package com.example.mira.briskula;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mira on 02.04.16..
 */
public class Opcije extends Activity {
    int height, width, iteracije;
    ImageButton[] buttoni = new ImageButton[3];
    AbsoluteLayout ekran;
    float kraj_x, kraj_y, pocetak_x, pocetak_y;
    Dialog dialog;
    int karta1, karta2, spil;

    private void dimenzijeEkrana(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    private void ucitajButtone(){
        buttoni[0] = (ImageButton) findViewById(R.id.help);
        buttoni[1] = (ImageButton) findViewById(R.id.options);
        buttoni[2] = (ImageButton) findViewById(R.id.home);
    }

    private void postaviDimenzije() {
        for (int i = 0; i < 3; ++i){
            buttoni[i].setAdjustViewBounds(true);
            buttoni[i].setMaxHeight((int) Math.round(0.2 * height));
            buttoni[i].setMaxWidth( (int) Math.round(0.3 * width));
        }
        BitmapDrawable bd=(BitmapDrawable) this.getResources().getDrawable(R.drawable.help);
        int h=bd.getBitmap().getHeight();
        int w=bd.getBitmap().getWidth();
        if( h > (int) Math.round(0.2 * height)) {
            Log.d("briskula", "tu");
            h = (int) Math.round(0.2 * height);
            w = (int) Math.round(0.3 * width);
        }
        Log.d("briskula", Integer.toString(width) + " " + Integer.toString(height));
        int udaljenost = ( height - 3 * h) / 4;
        for( int i = 0; i < 3; ++i ){
            buttoni[i].setX( ( width - w ) / 2);
            buttoni[i].setY(h * i + udaljenost * (i + 1));
            Log.d("briskula", Integer.toString(( width - w ) / 2) + " " + Integer.toString(h * i + udaljenost * (i + 1)));
        }
        Log.d("briskula", Integer.toString(h) + " " + Integer.toString(w) + " " + udaljenost);

    }

    private void postaviTouchNaEkran(){
        ekran.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pocetak_x = event.getRawX();
                        pocetak_y = event.getRawY();
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        return true;

                    case MotionEvent.ACTION_UP:
                        kraj_x = event.getRawX();
                        kraj_y = event.getRawY();
                        if (kraj_x - pocetak_x > 0.15 * width) {
                            finish();
                            overridePendingTransition(R.anim.slide_left1, R.anim.slide_right1);
                        }
                        return true;

                }
                return false;
            }
        });
    }

    private void dialogPomoc(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_pomoc);
        dialog.setTitle("Pomoc!");

        ImageView you = (ImageView) dialog.findViewById(R.id.You_izasla);
        ImageView comp = (ImageView) dialog.findViewById(R.id.Comp_izasla);
        if ( karta1 != -1 ) {
            you.setImageResource(Single_play.getIds().get(karta1));
            comp.setImageResource(Single_play.getIds().get(karta2));
        }
        TextView koliko = (TextView) dialog.findViewById(R.id.koliko_spil);
        koliko.setText("U špilu je ostalo " + Integer.toString(spil));
        Button OK = (Button) dialog.findViewById(R.id.ok);
        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opcije);

        ekran = (AbsoluteLayout)findViewById(R.id.opcije_ekran);
        Intent i = getIntent();
        iteracije = i.getIntExtra("iteracije", 200);
        karta1 = i.getIntExtra("karta1", -1);
        karta2 = i.getIntExtra("karta2", -1);
        spil = i.getIntExtra("spil", -1);

        dimenzijeEkrana();
        ucitajButtone();
        postaviDimenzije();
        postaviTouchNaEkran();
        dialogPomoc();
    }

    public void help( View view ){
       dialog.show();
    }

    public void options( View view ){
        ///dialog... za promjenu boje pozadine, itd...
    }

    public void home( View view ){
        Intent i = new Intent("com.example.mira.MainActivity");
        startActivity(i);
    }
}
