package com.example.mira.briskula;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mira on 19.2.2016..
 */

///ideja covjek upravlja svime
public class Single_play extends Activity {
    MCBriskula briska;          //klasa briskula koja se brine o stanju cijele igre
    ImageButton  b, opposite;
    ImageButton[] covjek_karte = new ImageButton[3];
    static List<Integer> ids;
    TextView txt;
    AbsoluteLayout ekran;
    View.OnTouchListener l1, l2, l0;
    int odabir_covjeka, height, width, comp_odluka, iteracije;
    android.os.Handler handler;
    MotionEvent global_e;
    ImageButton[] comp_karte = new ImageButton[3];
    float x0, x1, x2, x_brskula, x_opp, x_baci_covjek, x_baci_comp; //x koordinate karata
    float y1, y2, y_briskula, y_opp, y_baci_covjek, y_baci_comp; //y koordinate karata
    float pocetak_x,kraj_x, pocetak_y, kraj_y;
    Dialog kraj_igre;
    AlertDialog savjet;
    final Context context = this;

    ///koristimo za rekurzivne animacije pri postavljanju stola na pocetku igre
    ImageButton[] pomoc_polje = new ImageButton[8];
    float[] pomoc_rotacije = new float[8];
    float[] pomoc_x_konacno = new float[8];
    float[] pomoc_y_konacno = new float[8];
    float[] pomoc_scale = new float[8];

    class Pomoc implements Runnable{
        int tmp;
        public void iscrtaj_karte() {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (briska.pobjednik == 0) {
                        ///onda je pobjedio covjek i karte idu prema dolje
                        comp_karte[comp_odluka].animate().y(height + 300).rotation(0).setDuration(200).setListener(null);
                        covjek_karte[odabir_covjeka].animate().y(height + 300).rotation(0).setDuration(200).setListener(null);
                    } else {
                        comp_karte[comp_odluka].animate().y(-300).setDuration(200).rotation(0).setListener(null);
                        covjek_karte[odabir_covjeka].animate().y(-300).setDuration(200).rotation(0).setListener(null);
                    }
                }
            }, 3000);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    comp_karte[comp_odluka].setImageResource(R.drawable.opposite1);
                    if (briska.karte_igraca.get(0).size() > 0) {
                        comp_karte[0].setX(x0);
                        comp_karte[0].setY(y1);
                        covjek_karte[0].setX(x0);
                        covjek_karte[0].setY(y2);
                        covjek_karte[0].setImageResource(ids.get(briska.karte_igraca.get(0).get(0)));
                        covjek_karte[1].setVisibility(View.GONE);
                        comp_karte[1].setVisibility(View.GONE);
                        covjek_karte[2].setVisibility(View.GONE);
                        comp_karte[2].setVisibility(View.GONE);
                        if(briska.pobjednik==0 && briska.karte_igraca.get(0).size() == 1) {
                            covjek_karte[0].setOnTouchListener(l0);
                            covjek_karte[1].setOnTouchListener(l1);
                            covjek_karte[2].setOnTouchListener(l2);
                        }
                        if(briska.karte_igraca.get(0).size() == 1 ||briska.karte_igraca.get(0).size() == 2 || briska.karte.size()==0){
                            opposite.setVisibility(View.GONE);
                            b.setVisibility(View.GONE);
                        }
                        if (briska.karte_igraca.get(0).size() > 1) {
                            comp_karte[1].setX(x1);
                            comp_karte[1].setY(y1);
                            covjek_karte[1].setVisibility(View.VISIBLE);
                            comp_karte[1].setVisibility(View.VISIBLE);
                            covjek_karte[1].setX(x1);
                            covjek_karte[1].setY(y2);
                            covjek_karte[1].setImageResource(ids.get(briska.karte_igraca.get(0).get(1)));
                            if(briska.pobjednik==0 && briska.karte_igraca.get(0).size() == 2) {
                                covjek_karte[0].setOnTouchListener(l0);
                                covjek_karte[1].setOnTouchListener(l1);
                                covjek_karte[2].setOnTouchListener(l2);
                            }

                            if(briska.karte_igraca.get(0).size()>2 ){
                                covjek_karte[2].setVisibility(View.VISIBLE);
                                comp_karte[2].setVisibility(View.VISIBLE);
                                comp_karte[2].setX(x_opp);
                                comp_karte[2].setY(y_opp);
                                comp_karte[2].setRotation(90);
                                covjek_karte[2].setX(x_opp);
                                covjek_karte[2].setY(y_opp);
                                covjek_karte[2].setRotation(90);
                                covjek_karte[2].setImageResource(R.drawable.opposite1);
                                if(briska.pobjednik==1){
                                    comp_karte[2].animate().x(x2).y(y1).rotation(0).setDuration(400).setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {}

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                                covjek_karte[2].animate().x(x2).y(y2).setDuration(400).rotation(0).scaleY(-1).setListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {}

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        covjek_karte[2].setScaleY(1);
                                                        covjek_karte[2].setImageResource(ids.get(briska.karte_igraca.get(0).get(2)));
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {}

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {}
                                                });
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {}

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {}
                                    });
                                }
                                else{
                                    covjek_karte[2].animate().x(x2).y(y2).scaleY(-1).rotation(0).setDuration(400).setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {}

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            covjek_karte[2].setScaleY(1);
                                            covjek_karte[2].setImageResource(ids.get(briska.karte_igraca.get(0).get(2)));
                                            comp_karte[2].animate().x(x2).y(y1).rotation(0).setDuration(400).setListener(new Animator.AnimatorListener() {
                                                @Override
                                                public void onAnimationStart(Animator animation) {}

                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                        covjek_karte[0].setOnTouchListener(l0);
                                                        covjek_karte[1].setOnTouchListener(l1);
                                                        covjek_karte[2].setOnTouchListener(l2);
                                                }

                                                @Override
                                                public void onAnimationCancel(Animator animation) {}

                                                @Override
                                                public void onAnimationRepeat(Animator animation) {}
                                            });

                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {}

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {}
                                    });
                                }
                            }
                        }
                    }
                }
            }, 4000);
        }
        @Override
        public void run(){
            final int min = 0, max = 90;
            tmp = briska.pobjednik;
            if(odabir_covjeka==0)
                briska.odigrao(0);
            if(odabir_covjeka==1)
                briska.odigrao(1);
            if(odabir_covjeka==2)
                briska.odigrao(2);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    covjek_karte[odabir_covjeka].animate().x(x_baci_covjek).y(y_baci_covjek).rotation(min + (int) (Math.random() * max)).
                            setDuration(100).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (tmp == 0) {
                                comp_odluka = briska.igra_comp();
                                comp_karte[comp_odluka].animate().x(x_baci_comp).y(y_baci_comp).scaleY(-1).setDuration(100).rotation(min + (int) (Math.random() * max))
                                     .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        comp_karte[comp_odluka].setScaleY(1);
                                        comp_karte[comp_odluka].setImageResource(ids.get(briska.izasle.get(briska.izasle.size() - 1)));
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {
                                    }

                                });
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                }
            });

            iscrtaj_karte();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (briska.pobjednik == 1 && briska.karte_igraca.get(0).size() != 0) {
                        comp_odluka = briska.igra_comp();
                        comp_karte[comp_odluka].animate().x(x_baci_comp).y(y_baci_comp).rotation(min + (int) (Math.random() * max)).
                                scaleY(-1).setDuration(100).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                comp_karte[comp_odluka].setScaleY(1);
                                comp_karte[comp_odluka].setImageResource(ids.get(briska.izasle.get(briska.izasle.size() - 1)));
                                covjek_karte[0].setOnTouchListener(l0);
                                covjek_karte[1].setOnTouchListener(l1);
                                covjek_karte[2].setOnTouchListener(l2);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

                    }
                }
            }, 5000);
            if(briska.karte_igraca.get(0).size() == 0) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (briska.pobjednik == 0) {
                            ///onda je pobjedio covjek i karte idu prema dolje
                            comp_karte[comp_odluka].animate().y(height + 300).setDuration(500).setListener(null);
                            covjek_karte[odabir_covjeka].animate().y(height + 300).setDuration(500).setListener(null);
                        } else {
                            comp_karte[comp_odluka].animate().y(-300).setDuration(500).setListener(null);
                            covjek_karte[odabir_covjeka].animate().y(-300).setDuration(500).setListener(null);
                        }
                    }
                }, 6000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        comp_karte[0].setVisibility(View.GONE);
                        comp_karte[1].setVisibility(View.GONE);
                        comp_karte[2].setVisibility(View.GONE);
                        covjek_karte[0].setVisibility(View.GONE);
                        covjek_karte[1].setVisibility(View.GONE);
                        covjek_karte[2].setVisibility(View.GONE);
                        dialogKrajIgre();
                        kraj_igre.show();
                    }
                },7000);
            }


        }
    }


    class PodjelaKarataNaPocetkuIgre implements Runnable {

        private void rekurzivnaAnimacija( final int idx ){
            if( idx == 8 ) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        final int min = -15, max = 30, tmp_brojac1 = idx;
                        comp_odluka = briska.igra_comp();
                        Log.d("briskula", Integer.toString(comp_odluka) + "comp bacaAAA "+ Float.toString(comp_karte[comp_odluka].getScaleY()));
                        comp_karte[comp_odluka].animate().x(x_baci_comp).y(y_baci_comp).scaleY(-1).setDuration(100).rotation(min + (int) (Math.random() * max))
                                .setListener(new Animator.AnimatorListener() {
                                    int tmp_brojac2 = tmp_brojac1;

                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        //Log.d("briskula", "zapoceo sam animaciju kada baca komp");
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        comp_karte[comp_odluka].setScaleY(1);
                                        //Log.d("briskula", "kraj animacije kada baca komp");
                                        comp_karte[comp_odluka].setImageResource(ids.get(briska.izasle.get(briska.izasle.size() - 1)));
                                        covjek_karte[0].setOnTouchListener(l0);
                                        covjek_karte[1].setOnTouchListener(l1);
                                        covjek_karte[2].setOnTouchListener(l2);
                                        rekurzivnaAnimacija(++tmp_brojac2);
                                        dialogSavjet();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                    }
                });
            }
            else if( idx < 8 ) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("briskula", "inace");
                        final int tmp_brojac1 = idx;
                        pomoc_polje[tmp_brojac1].animate().scaleY(pomoc_scale[tmp_brojac1]).x(pomoc_x_konacno[tmp_brojac1]).y(pomoc_y_konacno[tmp_brojac1])
                                .rotation(pomoc_rotacije[tmp_brojac1]).setDuration(200).setListener(new Animator.AnimatorListener() {
                            int tmp_brojac2 = tmp_brojac1;

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Log.d("briskula", "animiram " + Integer.toString(tmp_brojac2));
                                pomoc_polje[tmp_brojac2].setScaleY(1);
                                if (tmp_brojac2 < 3)
                                    pomoc_polje[tmp_brojac2].setImageResource(ids.get(briska.karte_igraca.get(0).get(tmp_brojac2)));
                                else if (tmp_brojac2 == 6)
                                    pomoc_polje[tmp_brojac2].setImageResource(ids.get(briska.briskula));
                                rekurzivnaAnimacija(++tmp_brojac2);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }
                });
            }
            else
                return;
        }
        @Override
        public void run() {
            rekurzivnaAnimacija(0);
        }

    }

    //sto ce se dogoditi kada korisnik klikne na odabranu kartu
    private void onTouchListneri(){

        l0 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {
                odabir_covjeka = 0;
                global_e = m;
                Log.d("briskula", "novi klik1");
                covjek_karte[0].setOnTouchListener(null);
                covjek_karte[1].setOnTouchListener(null);
                covjek_karte[2].setOnTouchListener(null);
                new Thread(new Pomoc()).start();
                return true;
            }
        };
        l1 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                odabir_covjeka = 1;
                global_e = e;
                Log.d("briskula", "novi klik2");
                covjek_karte[0].setOnTouchListener(null);
                covjek_karte[1].setOnTouchListener(null);
                covjek_karte[2].setOnTouchListener(null);
                new Thread(new Pomoc()).start();
                return true;
            }
        };
        l2 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                odabir_covjeka = 2;
                global_e = e;
                Log.d("briskula", "novi klik3");
                covjek_karte[0].setOnTouchListener(null);
                covjek_karte[1].setOnTouchListener(null);
                covjek_karte[2].setOnTouchListener(null);
                new Thread(new Pomoc()).start();
                return true;
            }
        };
        //ovo cemo na kraju animacije za pocetnu podjelu karata
        /*covjek_karte[0].setOnTouchListener(l0);
        covjek_karte[1].setOnTouchListener(l1);
        covjek_karte[2].setOnTouchListener(l2);*/
    }

    private void ucitajSlike(){
        ids = new ArrayList<Integer>();
        ids.add(0, R.drawable.slika0);
        ids.add(1, R.drawable.slika1);
        ids.add(2, R.drawable.slika2);
        ids.add(3, R.drawable.slika3);
        ids.add(4, R.drawable.slika4);
        ids.add(5, R.drawable.slika5);
        ids.add(6, R.drawable.slika6);
        ids.add(7, R.drawable.slika7);
        ids.add(8, R.drawable.slika8);
        ids.add(9, R.drawable.slika9);
        ids.add(10, R.drawable.slika10);
        ids.add(11, R.drawable.slika11);
        ids.add(12, R.drawable.slika12);
        ids.add(13, R.drawable.slika13);
        ids.add(14, R.drawable.slika14);
        ids.add(15, R.drawable.slika15);
        ids.add(16, R.drawable.slika16);
        ids.add(17, R.drawable.slika17);
        ids.add(18, R.drawable.slika18);
        ids.add(19, R.drawable.slika19);
        ids.add(20, R.drawable.slika20);
        ids.add(21, R.drawable.slika21);
        ids.add(22, R.drawable.slika22);
        ids.add(23, R.drawable.slika23);
        ids.add(24, R.drawable.slika24);
        ids.add(25, R.drawable.slika25);
        ids.add(26, R.drawable.slika26);
        ids.add(27, R.drawable.slika27);
        ids.add(28, R.drawable.slika28);
        ids.add(29, R.drawable.slika29);
        ids.add(30, R.drawable.slika30);
        ids.add(31, R.drawable.slika31);
        ids.add(32, R.drawable.slika32);
        ids.add(33, R.drawable.slika33);
        ids.add(34, R.drawable.slika34);
        ids.add(35, R.drawable.slika35);
        ids.add(36, R.drawable.slika36);
        ids.add(37, R.drawable.slika37);
        ids.add(38, R.drawable.slika38);
        ids.add(39, R.drawable.slika39);
    }

    private void postaviKarte(){
        covjek_karte[0] = (ImageButton)findViewById(R.id.karta0);
        covjek_karte[1] = (ImageButton)findViewById(R.id.karta1);
        covjek_karte[2] = (ImageButton)findViewById(R.id.karta2);
        comp_karte[0] = (ImageButton)findViewById(R.id.c_karta0);
        comp_karte[1] = (ImageButton)findViewById(R.id.c_karta1);
        comp_karte[2] = (ImageButton)findViewById(R.id.c_karta2);
        b = (ImageButton)findViewById(R.id.briskula);
        opposite = (ImageButton)findViewById(R.id.opp);
        txt = (TextView)findViewById(R.id.text);

        for(int i = 0; i < 3; ++i) {
            covjek_karte[i].setImageResource(R.drawable.opposite1);
            comp_karte[i].setImageResource(R.drawable.opposite1);
        }
        opposite.setImageResource(R.drawable.opposite1);
        b.setImageResource(R.drawable.opposite1);
    }

    //dobivamo veličinu ekrana u pixelima
    private void dimenzijeEkrana(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    private void postaviPomocnaPolja(){
        for( int i = 0; i < 3; ++i ){
            pomoc_polje[i] = covjek_karte[i];
            pomoc_polje[i+3] = comp_karte[i];
        }
        pomoc_polje[7] = opposite;
        pomoc_polje[6] = b;
        for( int i = 0; i < 8; ++i ) {
            pomoc_polje[i].setX((float) 0.40 * width);
            pomoc_polje[i].setY((float) 0.38 * height);
            pomoc_polje[i].setRotation(90);
            pomoc_polje[i].setPadding(0, 0, 0, 0);
            if( i!= 7 ) {
                pomoc_rotacije[i] = 0;
                pomoc_scale[i] = -1;
            }
            else{
                pomoc_rotacije[i] = 90;
                pomoc_scale[i] = pomoc_polje[i].getScaleY();
            }
            pomoc_polje[i].setAdjustViewBounds(true); //da bi mogli primjeniti maxheight
            pomoc_polje[i].setMaxHeight((int) Math.round(0.44 * height));
            pomoc_polje[i].setMaxWidth((int) Math.round(0.21* width));
        }

        BitmapDrawable bd=(BitmapDrawable) this.getResources().getDrawable(R.drawable.opposite1);
        int h=bd.getBitmap().getHeight();
        int w=bd.getBitmap().getWidth();
        if( h > (int) Math.round(0.44 * height) ){
            h = (int) Math.round(0.44 * height);
            w = (int) Math.round(0.21* width);
        }
        //Log.d("briskula", "izrACUNAO SAM DIMENZIJE "+ Integer.toString( w ) + " ... " + Integer.toString( h ));
        float udaljenost = width-3 * w;
        udaljenost/=4;
        for( int i = 0; i < 6; ++i){
            pomoc_x_konacno[i] = ( i % 3 + 1 )*udaljenost + ( i % 3 ) * w;
            pomoc_y_konacno[i] = (i < 3) ? ( (float ) 0.78 * height ) : ( ( float )-0.3 * h);
        }
        pomoc_x_konacno[7] = (float) 0.10 * width;
        pomoc_y_konacno[7] = (float) 0.30 * height;
        pomoc_x_konacno[6] = (float) 0.1 * width;
        pomoc_y_konacno[6] = (float) 0.30 * height;
        x0 = udaljenost;
        x1 = 2*udaljenost+w;
        x2 = 3*udaljenost+2 * w;
        y1=(float)-0.3 * h;
        y2=(float)0.78 * height;
        x_brskula =(float) 0.1*width;
        y_briskula=(float) 0.30*height;
        x_opp = (float)0.10*width;
        y_opp = (float)0.30*height;
        x_baci_comp = (float)0.60*width;
        y_baci_comp =(float)0.30*height;
        x_baci_covjek = (float)0.65*width;
        y_baci_covjek = (float)0.50*height;

    }

    private void dialogKrajIgre(){
        kraj_igre = new Dialog(context);
        kraj_igre.setContentView(R.layout.end);
        ImageButton newGame = (ImageButton)kraj_igre.findViewById(R.id.new_game);
        ImageButton home = (ImageButton)kraj_igre.findViewById(R.id.home);
        ImageButton end = (ImageButton)kraj_igre.findViewById(R.id.end_game);
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.Single_play");
                i.putExtra("iteracija", iteracije);
                startActivity(i);
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.MainActivity");
                startActivity(i);
            }
        });
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///izaci iz aplikacije
            }
        });

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView text = (TextView)kraj_igre.findViewById(R.id.dialog_kraj_igre_text);
        text.setText("YOU: " + Integer.toString(briska.bodovi.get(0)) + "  " + "DEVICE: " + Integer.toString(briska.bodovi.get(1)));
    }

    private void dialogSavjet(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        savjet = alertDialogBuilder.create();
        savjet.setTitle("Tip");
        savjet.setMessage("Slide left for more options");
        savjet.show();
    }

    private void postaviTouchNaEkran(){
        ekran.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pocetak_x = event.getRawX();
                        pocetak_y = event.getRawY();
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        return true;

                    case MotionEvent.ACTION_UP:
                        kraj_x = event.getRawX();
                        kraj_y = event.getRawY();
                        if (kraj_x - pocetak_x < -0.15 * width) {
                            Intent i = new Intent("com.example.mira.Opcije");
                            i.putExtra("briskula", iteracije);
                            if( briska.izasle.size() > 2 ) {
                                i.putExtra("karta1", briska.izasle.get(briska.izasle.size() - 1));
                                i.putExtra("karta2", briska.izasle.get(briska.izasle.size() - 2));
                            }
                            else{
                                i.putExtra("karta1", -1);
                                i.putExtra("karta2", -1);
                            }
                            i.putExtra("spil", briska.karte.size());
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
                        }
                        return true;

                }
                return false;
            }
        });
    }

    static public List<Integer> getIds(){
        return ids;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_play);

        //pomocu handlera mijenjamo UI iz drugih dretvi
        handler = new android.os.Handler(Looper.getMainLooper());
        ekran = (AbsoluteLayout)findViewById(R.id.ekran);

        Intent intent = getIntent();
        iteracije = intent.getIntExtra("briskula", 200);

        postaviKarte();
        ucitajSlike();
        onTouchListneri();
        dimenzijeEkrana();

        //stvaramo novu igru, definiramo broj iteracija za MC algoritam, broj igraca i broj_karti
        briska = new MCBriskula(iteracije, 2, 3);
        for(int i=0; i<2; ++i)
            briska.podjeli_karte_na_pocetku(i);
        briska.postavi_briskulu();

        postaviPomocnaPolja();

        postaviTouchNaEkran();

        new Thread(new PodjelaKarataNaPocetkuIgre()).start();
    }
}
