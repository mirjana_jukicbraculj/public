package com.example.mira.briskula;


import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;

/**
 * Created by Mira on 18.2.2016..
 */
public class Single_stage extends Activity {

    ImageButton[] opcije = new ImageButton[6];
    float[] rotacije = new float[6];
    float[] x_konacno = new float[6];
    float[] y_konacno = new float[6];
    int width, height;
    android.os.Handler handler;

    //pronalazi buttone definirane u single_stage.xml
    private void ucitaj_opcije(){
        opcije[0] = (ImageButton)findViewById(R.id.easy2);
        opcije[1] = (ImageButton)findViewById(R.id.easy4);
        opcije[2] = (ImageButton)findViewById(R.id.medimu2);
        opcije[3] = (ImageButton)findViewById(R.id.medimu4);
        opcije[4] = (ImageButton)findViewById(R.id.hard2);
        opcije[5] = (ImageButton)findViewById(R.id.hard4);
    }

    //pronalazi dimenzije ekrana, potrebno da znamo pozicionirati elemente (x, y) kooridate
    private void dimenzije_ekrana(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    private void postavi_dimenzije(){
        int min = -20, max = 40;
        float brojac = 0;
        for( int i = 0; i < 6; ++i){
            opcije[i].setX(-(float)0.5*width);
            opcije[i].setY((float) 0.4 * height);
            rotacije[i] = min + (int)(Math.random() * max);
            x_konacno[i] = (float) 0.03 * width + (float)0.5* width * ( i % 2 );
            y_konacno[i] = (float) 0.10 *height + brojac;
            if( i % 2 == 1) brojac += (float)0.3*height;
        }
    }


    class animiraj_opcije implements Runnable{

        private void rekurzivna_animacija( final int brojac ){
            if( brojac == 6 )
                return;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    final int tmp_brojac1 = brojac;
                    opcije[tmp_brojac1].animate().rotation(rotacije[tmp_brojac1]).x(x_konacno[tmp_brojac1]).y(y_konacno[tmp_brojac1]).setDuration(150)
                            .setListener(new Animator.AnimatorListener() {
                                int tmp_brojac2 = tmp_brojac1;
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    Log.d("briskula", "pomakunuo sam "+Integer.toString(tmp_brojac2));
                                    rekurzivna_animacija(++tmp_brojac2);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                }
            });
        }
        @Override
        public void run(){
            //rekurzivno animiramo pojavu opcija na ekranu
            rekurzivna_animacija(0);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("briskula", "2 stage");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_stage);

        handler = new android.os.Handler(Looper.getMainLooper());
        Log.d("briskula", "2 stage");
        ucitaj_opcije();
        dimenzije_ekrana();
        Log.d("briskula", "2 stage");
        postavi_dimenzije();
        Thread nova_dretva = new Thread( new animiraj_opcije() );
        nova_dretva.start();

    }





    public void easy2(View view){
        Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 200);
        startActivity(i);
    }
    public void easy4(View view){
       /* Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 2000);
        startActivity(i);*/
    }
    public void medium2(View view){
        Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 500);
        startActivity(i);
    }
    public void medium4(View view){
        /*Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 2000);
        startActivity(i);*/
    }
    public void hard2(View view){
        Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 1000);
        startActivity(i);
    }
    public void hard4(View view){
        /*Intent i = new Intent("com.example.mira.Single_play");
        i.putExtra("iteracija", 2000);
        startActivity(i);*/
    }

}
