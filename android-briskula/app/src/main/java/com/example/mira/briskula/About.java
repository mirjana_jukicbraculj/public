package com.example.mira.briskula;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by mira on 14.05.16..
 */
public class About extends Activity {
    @Override
    protected void onCreate( Bundle savedInstanceState ){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}
