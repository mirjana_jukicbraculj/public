package com.example.mira.briskula;
import android.util.Log;

import com.example.mira.briskula.MCBriskula;
import com.example.mira.briskula.Node;

import java.util.Random;

/**
 * Created by Mira on 19.2.2016..
 */
public class UCT {
    public int izracunaj(MCBriskula tmp){
        Node prazan = new Node();
        int brojac;
        tmp.print();
        Node rootnode = new Node(-3, prazan, tmp);
        int pobjednik = tmp.pobjednik;
        for(int i=0; i<tmp.iteracije; ++i){
            Node node = rootnode;
            brojac=0;
            MCBriskula state=tmp.Clone();
            while(node.untriesMoves.isEmpty() && !node.childNodes.isEmpty()) {
                node = node.UCTSelectChild();
                state.DoMove(node.move);
                ++brojac;
            }
            if(!node.untriesMoves.isEmpty()){
                Random r = new Random();
                Integer index = r.nextInt(node.untriesMoves.size());
                Integer m = node.untriesMoves.get(index);
                state.DoMove(m);
                node = node.AddChild(m, state);
                ++brojac;
            }
            while(!state.GetMoves().isEmpty()){
                Random r = new Random();
                Integer index = r.nextInt(state.GetMoves().size());
                Integer izbacujem_kartu = state.GetMoves().get(index);
                state.DoMove(izbacujem_kartu);
                ++brojac;
            }
            while(node.move!=-1){
                if(node.parentNode.move!=-1)
                    node.Update(state.GetResult(node.parentNode.player_na_potezu));
                else
                    node.Update(state.GetResult(1));
                node=node.parentNode;
            }


        }
        Node vracam = new Node();
        Double maxi = -1.0;
        for(int i=0; i<rootnode.childNodes.size();++i){
            if(maxi < rootnode.childNodes.get(i).visits){
                maxi = rootnode.childNodes.get(i).visits;
                vracam = rootnode.childNodes.get(i);
            }
        }
        return vracam.move;
    }
}
