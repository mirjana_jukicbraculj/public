package com.example.mira.briskula;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

/**
 * Created by mira on 13.05.16..
 */
public class Multi_register extends Activity {
    android.os.Handler handler;
    String username, password, mail;

    @Override
    protected void onCreate( Bundle savedInstanceState ){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multiplayer_register);
    }

    public void register( View view ){

        //idemo u novu activity
        Intent i = new Intent("com.example.mira.Multi_wait");
        EditText user = (EditText)findViewById(R.id.username);
        EditText pass = (EditText)findViewById(R.id.pass);
        EditText email = (EditText)findViewById(R.id.email);
        i.putExtra( "username", user.getText().toString());
        i.putExtra("pass", pass.getText().toString());
        i.putExtra("email", email.getText().toString());

        Log.d("briskula", "sve spremno");
        startActivity(i);
        //new Thread( new spajanje() ).start();
    }
}
