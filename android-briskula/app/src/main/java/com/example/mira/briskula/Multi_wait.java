package com.example.mira.briskula;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by mira on 12.05.16..
 */
public class Multi_wait extends Activity {

    android.os.Handler handler;
    String username, password, email;
    protected static final SharedPreferences settings = null;
    CookieManager cookieManager;
    Button[] onlines;
    Context contex;
    LinearLayout l;
    Button logout;
    int kraj_onlines=0;
    Thread onlines_t;
    String call;

    @Override
    protected void onCreate( Bundle savedInstanceState ){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multiplayer_wait);

        handler = new android.os.Handler();
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("pass");
        email = getIntent().getStringExtra("email");

        cookieManager = new java.net.CookieManager();
        CookieHandler.setDefault(cookieManager);
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        contex = this;
        l = (LinearLayout)findViewById(R.id.glavni_ekran);

        Log.d("briskula", "pokrecem novu dretvu");
        new Thread(new login()).start();

    }

    //funcija pretvara dobivene parametre u post dio url adrese
    // funkcija vraća dobiveni dio url-a
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    //funkcija dobiva podatke i dio url adrese na koju se spaja
    //funcija vraća string odgovor koji je učitan s dobivene url adrese
    private String komuniciraj(HashMap<String, String> mapa, String data){
        try{
            URL url = new URL(data);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            if(!mapa.isEmpty()) {
                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(mapa));
                writer.flush();
                writer.close();
                os.close();
            }


            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStream is = connection.getInputStream();
                InputStreamReader isReader = new InputStreamReader(is, "UTF-8");
                BufferedReader reader = new BufferedReader(isReader);
                String result = "";
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                String rez = URLEncoder.encode(result, "UTF-8");
                return  Html.fromHtml(result).toString();
            }
            else{
                Log.d("briskula", Integer.toString(connection.getResponseCode()));
                Log.d("briskula", connection.getResponseMessage());
            }
        }

        catch(IOException e){
            Log.d("php_spajanje", "greska_IO");
            e.printStackTrace();
        }
        catch(Exception e){
            Log.d("php_spajanje", "greska");
            e.printStackTrace();
        }
        return "greska";
    }

    ///drtva sluzi za logiranje/registraciju na server strani aplikacije
    private class login implements Runnable{

        @Override
        public void run(){
            final String odgovor;
            HashMap<String, String> mapa = new HashMap<String, String>();

            mapa.put("username", username);
            mapa.put("password", password);

            if( email == null ) {
                //String data = "http://192.168.0.24/~mira/server/index.php?rt=prijava/login";
                String data = "http://briskula.pe.hu/server/index.php?rt=prijava/login";
                odgovor = komuniciraj(mapa, data);
            }

            else{
                //String data = "http://192.168.0.24/~mira/server/index.php?rt=prijava/registracija";
                String data = "http://briskula.pe.hu/server/index.php?rt=prijava/registracija";
                mapa.put("email", email);
                odgovor = komuniciraj(mapa, data);
            }
            ispisOnlines(odgovor);

            onlines_t = new Thread(new onlines());
            onlines_t.start();
        }
    }

    //igracu koji nas je pozvao na igranje odgovaramo da ne prihvacamo njegov poziv
    private class odbij implements Runnable{
        @Override
        public void run(){
            String data = "http://briskula.pe.hu/server/index.php?rt=online/obradi_odgovor&username=" + username;
            HashMap<String, String> mapi = new HashMap<String, String>();
            mapi.put("odgovor", "ne");
            String odgovor = komuniciraj(mapi, data);
            synchronized (onlines_t) {
                onlines_t.notify();
            }
        }
    }

    //sluzi za dohvaćanje liste svoh online igraca
    private class onlines implements Runnable{
        @Override
        public void run(){
            while( true ) {
                final Thread ja = new Thread(this);
                //String data = "http://192.168.0.24/~mira/server/index.php?rt=online&username"+username;
                String data = "http://briskula.pe.hu/server/index.php?rt=online&username" + username;
                HashMap<String, String> mapp = new HashMap<>();
                final String odgovor1 = komuniciraj(mapp, data);
                if (odgovor1.indexOf("protiv") != -1) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(contex)
                                    .setTitle("Potvrda")
                                    .setMessage(odgovor1)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            new Thread(new odbij()).start();
                                        }
                                    })
                                    .show();

                        }
                    });
                    try{
                        synchronized (onlines_t) {
                            onlines_t.wait();
                        }
                    }
                    catch(Exception e){
                        Log.d("briskula", e.getMessage());
                    }
                } else {
                    ispisOnlines(odgovor1);

                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                        Log.d("briskula", e.getMessage());
                    }
                    if (kraj_onlines == 1)
                        break;
                }
            }
        }
    }
    //logout iz multiplayer dijela aplikacije
    private class logout implements Runnable{
        @Override
        public void run() {
            String data = "http://briskula.pe.hu/server/index.php?rt=prijava/logout&username=" + username;
            //String data = "http://192.168.0.24/~mira/server/index.php?rt=prijava/logout&username=" + username;
            HashMap<String, String> mapp = new HashMap<String, String>();
            String odgovor = komuniciraj(mapp, data);
            kraj_onlines = 1;
            Intent i = new Intent("com.example.mira.Multi_login");
            startActivity(i);
        }
    }

    /// odbijamo cekati igraca kojeg smo pozvali na igranje
    private class prekini implements Runnable{
        @Override
        public void run() {
            String data = "http://briskula.pe.hu/server/index.php?rt=online/odustani&username=" + username;
            //String data = "http://192.168.0.24/~mira/server/index.php?rt=prijava/logout&username=" + username;
            HashMap<String, String> mapp = new HashMap<String, String>();
            String odgovor = komuniciraj(mapp, data);
            synchronized (onlines_t) {
                onlines_t.notify();
            }
        }
    }

    ///cekamo da nas igrac kojeg smo izazvali
    private class cekaj implements Runnable{
        @Override
        public void run() {
            while(true){
                String data = "http://briskula.pe.hu/server/index.php?rt=online&username" + username;
                HashMap<String, String> mapp = new HashMap<>();
                final String odgovor1 = komuniciraj(mapp, data);
                if(odgovor1.indexOf("povući")==-1){
                    synchronized (onlines_t) {
                        onlines_t.notify();
                        break;
                    }
                }
            }
        }
    }
    ///pozivamo igraca na igru, username tog igraca je spremljen u varijablu call
    private class pozovi implements Runnable{
        @Override
        public void run() {
            String data = "http://briskula.pe.hu/server/index.php?rt=online/izazovi&username=" + username;
            //String data = "http://192.168.0.24/~mira/server/index.php?rt=prijava/logout&username=" + username;
            HashMap<String, String> mapp = new HashMap<String, String>();
            mapp.put("call",  call);
            final String odgovor1 = komuniciraj(mapp, data);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(contex)
                            .setTitle("Potvrda")
                            .setMessage(odgovor1)
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new Thread(new prekini()).start();
                                }
                            })
                            .show();

                }
            });
            try{
                synchronized (onlines_t) {
                    ///pokrenemo novu dretvu koja ce u pozadini cekati odgovor korisnika
                    new Thread(new cekaj()).start();
                    onlines_t.wait();
                }
            }
            catch(Exception e){
                Log.d("briskula", e.getMessage());
            }

        }
    }
    ///funcija vrsi update trenutne liste onlines igraca
    private void ispisOnlines(String odgovor1){
        String[] array1 = odgovor1.split("\n");
        final ArrayList<String> lista1 = new ArrayList<String>(Arrays.asList(array1));
        for(int i=0; i<lista1.size(); ++i)
            lista1.set(i, lista1.get(i).trim());

        lista1.removeAll(Arrays.asList(null, "", "\n"));
        handler.post(new Runnable() {
            @Override
            public void run() {
                l.removeAllViews();
                TextView tv = (TextView) new TextView(contex);
                tv.setText("Online korisnici");
                l.addView(tv);
                onlines = new Button[lista1.size() - 2];
                for (int i = 0; i < lista1.size() - 2; ++i) {
                    final int index = i;
                    onlines[i] = new Button(contex);
                    onlines[i].setText(lista1.get(i + 2));
                    l.addView(onlines[i]);
                    onlines[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            call = lista1.get(index+2);
                           new Thread(new pozovi()).start();
                        }
                    });
                }
                logout = new Button(contex);
                logout.setText("Log out");
                l.addView(logout);
                logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new logout()).start();
                    }
                });
            }
        });
    }

}
