package com.example.mira.briskula;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    ImageView[] slova = new ImageView[8];
    ImageButton[] opcije = new ImageButton[5];
    float[] x_konacno = new float[13];
    float[] y_konacno = new float[13];
    float[] rotacije = new float[13];
    int width, height;
    AbsoluteLayout l;
    android.os.Handler handler;
    Button[] buttoni = new Button[4];



    //pronalazi dimenzije ekrana, potrebno da znamo pozicionirati elemente (x, y) kooridate
    private void dimenzije_ekrana(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    //dinamicko dodavanje slova i buttona
    private void ucitaj_slova_i_opcije(){
        for( int i = 0; i < 8; ++i) {
            slova[i] = new ImageView(this);
            l.addView(slova[i]);
        }
        for( int i = 0; i < 5; ++i) {
            opcije[i] = new ImageButton(this);
            l.addView(opcije[i]);
        }
        slova[0].setImageResource(R.drawable.b);
        slova[1].setImageResource(R.drawable.r);
        slova[2].setImageResource(R.drawable.i);
        slova[3].setImageResource(R.drawable.s);
        slova[4].setImageResource(R.drawable.k);
        slova[5].setImageResource(R.drawable.u);
        slova[6].setImageResource(R.drawable.l);
        slova[7].setImageResource(R.drawable.a);
        opcije[0].setImageResource(R.drawable.opposite_single);
        opcije[1].setImageResource(R.drawable.opposite_multy);
        opcije[2].setImageResource(R.drawable.opposite_learn);
        opcije[3].setImageResource(R.drawable.opposite_options);
        opcije[4].setImageResource(R.drawable.opposite_end);

    }

    //postavlja x,y,rotaciju, konacni_x, konacni_y, koristimo u animacijama
    private void postavi_dimenzije(){
        for( int i = 0; i < 8; ++i){
            slova[i].setX((float) 0.45 * width);
            slova[i].setY((float)0.03*height);
            slova[i].setAdjustViewBounds(true); //da bi mogli primjeniti maxheight
            slova[i].setMaxHeight((int)Math.round(0.35*height));
            slova[i].setMaxWidth((int)Math.round(0.17*width));
            rotacije[i] = 13 * (float)(i-3.5);
            x_konacno[i] = slova[i].getX() + (float)0.115*width * (float)(i-3.5);
            y_konacno[i] = slova[i].getY() + 20 * (float)Math.abs(i-3.5);
        }
        float brojac = 0;
        int min = -15, max = 30;
        for( int i = 0; i < 5; ++i){
            opcije[i].setPadding(0,0,0,0);
            opcije[i].setX(-(float) 0.50 * width);
            opcije[i].setY((float) 0.20 * height);
            rotacije[i+8]= min + (int)(Math.random() * max); //rand vraca random broj između 0 i 1
            if( i != 4 )
                x_konacno[i+8] = (float) 0.03 * width + (float)0.5* width * ( i % 2 );
            else
                x_konacno[i+8] = (float) 0.30 * width   ;
            y_konacno[i+8] = (float) 0.40 *height + brojac;
            if( i % 2 == 1) brojac += (float) 0.17 * height;
        }
    }

    private void postavi_onClick(){
        opcije[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent("com.example.mira.Single_stage");
                startActivity(i);
            }
        });
        opcije[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.Multi_login");
                startActivity(i);
            }
        });
        opcije[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.mira.Learn");
                startActivity(i);
            }
        });
        opcije[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    class animiraj_slova_i_opcije implements Runnable{
        //rekurzivno animiramo postavljanje slova, handler prenosi UI dretvi slikanje karata
        private void rekurzivna_animacija(final int index){
            if(index == 13 )
                    return;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    final int moj_brojac = index;
                    if (moj_brojac < 8) {
                        slova[moj_brojac].animate().x(x_konacno[moj_brojac]).y(y_konacno[moj_brojac]).
                                rotation(rotacije[moj_brojac]).setDuration(80).setListener(new Animator.AnimatorListener() {
                            int index1 = moj_brojac;

                            @Override
                            public void onAnimationStart(Animator animation) {
                                Log.d("briskula", "zapoceo sam animaciju s brojacem" + Integer.toString(index1));
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Log.d("briskula", "zavrsio sam animaciju s brojacem" + Integer.toString(index1));
                                rekurzivna_animacija(++index1);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        });
                    } else {
                        opcije[moj_brojac-8].animate().x(x_konacno[moj_brojac]).y(y_konacno[moj_brojac]).
                                rotation(rotacije[moj_brojac]).setDuration(80).setListener(new Animator.AnimatorListener() {
                            int index1 = moj_brojac;

                            @Override
                            public void onAnimationStart(Animator animation) {
                                Log.d("briskula", "zapoceo sam animaciju s brojacem" + Integer.toString(index1));
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Log.d("briskula", "zavrsio sam animaciju s brojacem" + Integer.toString(index1));
                                rekurzivna_animacija(++index1);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        });
                    }
                }
             });

        }

        @Override
        public void run(){
            rekurzivna_animacija(0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new android.os.Handler(Looper.getMainLooper());
        l = (AbsoluteLayout)findViewById(R.id.abs_lay);

        dimenzije_ekrana();
        ucitaj_slova_i_opcije();
        postavi_onClick();
        postavi_dimenzije();
        Thread nova_dretva = new Thread( new animiraj_slova_i_opcije() );
        nova_dretva.start();

    }

}
