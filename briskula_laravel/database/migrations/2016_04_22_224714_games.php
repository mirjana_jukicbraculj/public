<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Games extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('games', function (Blueprint $table) {
            $table->string('email0');
            $table->string('email1');
            $table->string('email');
            $table->string('karte');
            $table->string('spil');
            $table->string('izasle');
            $table->string('briskula');
            $table->string('bodovi');
            $table->string('igrac_na_potezu');
            $table->string('odigrali');
            $table->string('bacene_karte');
            $table->string('pomoc');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('games');
    }
}
