<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::post('account', 'User@show');

Route::auth();

Route::get('/home', 'HomeController@index');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('/play', 'PlayController@start');
Route::post('/calling', 'PlayController@call');
Route::post('/play', 'PlayController@obradi');
Route::get('/cekanje', 'PlayController@cekanje');
Route::get('/igra', 'BriskulaController@start');
Route::get('/igraUTijeku', 'BriskulaController@igra_u_tijeku');
Route::post('/igraUTijekuPost', 'BriskulaController@igra_u_tijeku_post');
Route::get('/pomoc', 'BriskulaController@pomoc');

Route::controllers([
   'password' => 'Auth\PasswordController',
]);
