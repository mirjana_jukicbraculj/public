<?php

namespace App\Http\Controllers;


use Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Game;

class BriskulaController extends Controller
{

    const trenutni_broj_igraca = 2;

    public function start(){

    	if(Auth::user()->email === DB::table('onlines')->where('protivnik', Auth::user()->email)->value('protivnik')){
    
            $nisam_ja =  DB::table('onlines')->where('protivnik', Auth::user()->email)->value('email');

    		$briskula = new Game();
    		$email = array();
            array_push($email, Auth::user()->email);
            array_push($email, $nisam_ja);
	    	$briskula->email = implode(', ', $email);

            $briskula->email0 = Auth::user()->email;
            $briskula->email1 = $nisam_ja;

            $spil = range( 0, 39 );
	    	shuffle( $spil );
            $karte = array();
	    	$karte0 = array();
	    	for( $i = 0; $i < 3; $i++ )
	    		$karte0[$i] = array_pop($spil);
	    	$karte1 = array();
	    	for( $i = 0; $i < 3; $i++ )
	    		$karte1[$i] = array_pop($spil);

            $karte0 = implode(', ', $karte0);
            $karte1 = implode(', ', $karte1);

            array_push($karte, $karte0);
            array_push($karte, $karte1);
            $karte = implode(', ', $karte);
            $briskula->karte = $karte;

            $briskula->briskula = array_pop($spil);
            array_unshift($spil, $briskula->briskula);
            $izasle = array();
            $bacene_karte = array();

            $briskula->spil = implode(', ', $spil);
            $briskula->izasle = implode(', ', $izasle);
            $briskula->bacene_karte = implode(', ', $bacene_karte);

            $bodovi = array();
            array_push($bodovi, 0);
            array_push($bodovi, 0);
	    	$briskula->bodovi = implode(', ', $bodovi);
     
           // $briskula->igrac_na_potezu = $briskula->email0;
            $briskula->igrac_na_potezu = 0;
	 
        	$briskula->odigrali = 0;
	    	
	    	$briskula->save(); 	
	   	}
	    
    	return redirect('igraUTijeku');
    }

    public function igra_u_tijeku(){

    	$prvi = DB::table('games')->where('email0', Auth::user()->email)->first();
        $drugi = DB::table('games')->where('email1', Auth::user()->email)->first();
        if( !empty($prvi))
            $redak = $prvi;
        else if( !empty($drugi))
            $redak = $drugi;
    	else //ako jos nije inicijalizirana igra
    		return redirect('igraUTijeku');
    	
        //if(Auth::user()->email === $redak->email0)

        $redak = $this->provjera_kraja_runde($redak);

        if($this->provjeri_kraj_igre($redak)){

            //unisitit redak!
            //ponisitit onlines
            return view('play.kraj_igre')->with('redak', implode(', ',(array) $redak));
        
        }

        $karte = array();

    	if( $redak->email0 === Auth::user()->email)
            for($i=0; $i<3; $i++)
    	       	$karte[$i] = explode(', ',$redak->karte)[$i];
    	else 
    		for($i=0; $i<3; $i++)
                $karte[$i] = explode(', ',$redak->karte)[$i+3];

    	$email = explode(', ', $redak->email);

    	if($email[(int)$redak->igrac_na_potezu] === Auth::user()->email)
			return view('play.igra_u_tijeku_slike')->with('karte', $karte)->with('redak', implode(', ',(array)$redak));
		else
			return view('play.igra_u_tijeku_slike_disabled')->with('karte', $karte)->with('redak', implode(', ',(array)$redak));
    }

    /**
        Funkcija vraca koji igrac je sada na potezu
    */
    public function provjera_kraja_runde($redak){
    	if((int)$redak->odigrali === BriskulaController::trenutni_broj_igraca) {
            if( Auth::user()->email === $redak->email0 ){
                $stanje = $this->trenutno_uzima($redak);
                $broj_igraca_koji_uzima = (int)$stanje[0];
               // return view('play.pomoc')->with('pobjednik', $broj_igraca_koji_uzima);
                $broj_bodova_koji_uzima = (int)$stanje[1];

                $bodovi = explode(', ', $redak->bodovi);
                $bodovi[$broj_igraca_koji_uzima] = (int)$bodovi[$broj_igraca_koji_uzima] + (int)$broj_bodova_koji_uzima;
                $bodovi = implode(', ', $bodovi);

                $redak = $this->dodjeli_karte($redak);

                $redak->odigrali = '0';
                $redak->bacene_karte = '';
                $redak->igrac_na_potezu = $broj_igraca_koji_uzima;
                $redak->bodovi = $bodovi;

                DB::table('games')->where('email', $redak->email)->update(array('bodovi'=> $redak->bodovi));
                DB::table('games')->where('email', $redak->email)->update(array('karte'=>$redak->karte));
                DB::table('games')->where('email', $redak->email)->update(array('odigrali'=> $redak->odigrali));
                DB::table('games')->where('email', $redak->email)->update(array('bacene_karte'=>$redak->bacene_karte));
                DB::table('games')->where('email', $redak->email)->update(array('igrac_na_potezu'=> $redak->igrac_na_potezu));
                DB::table('games')->where('email', $redak->email)->update(array('spil'=> $redak->spil));  
                //DB::table('games')->where('email', $redak->email)->update(array('pomoc'=> $broj_igraca_koji_uzima));
                }
        }
        return $redak;

    }

    /**
    	Funkcija koja vraca mail igraca koji je uzeo rundu.
    */
    private function trenutno_uzima($redak){
    	$briskula = (int)$redak->briskula;
    	//$karte_string = explode(', ', $redak->bacene_karte);
        $karte = array_map('intval', explode(',', $redak->bacene_karte));

    	$najveca_briskula = (int)-1;
    	$najveca_prva_boja = (int)$karte[0];
        $bodovi = 0;

    	for($i=0; $i<BriskulaController::trenutni_broj_igraca; $i++){
    		if((int)($karte[$i]/10) == (int)($briskula/10) && $najveca_briskula < $karte[$i]) 
    			$najveca_briskula = $karte[$i];
    		if((int)($karte[$i]/10) == (int)($najveca_prva_boja/10) && $karte[$i] > $najveca_prva_boja){
    			$najveca_prva_boja = $karte[$i];
            }
            DB::table('games')->where('email', $redak->email)->update(array('pomoc'=> (int)($najveca_briskula/10)));
    	    $bodovi += $this->bodovi_karte($karte[$i]);
        }
           

    	if($najveca_briskula !== -1) {
    		for($i=0; $i<BriskulaController::trenutni_broj_igraca; $i++){
    			if($najveca_briskula == $karte[$i])
    				return array(($i+$redak->igrac_na_potezu)%BriskulaController::trenutni_broj_igraca, $bodovi); 
    		}
    	}

    	for($i=0; $i<BriskulaController::trenutni_broj_igraca; $i++){
    			if($najveca_prva_boja == $karte[$i])
    	           return array(($i+$redak->igrac_na_potezu)%BriskulaController::trenutni_broj_igraca, $bodovi); 
    	}

    }

    private function bodovi_karte($karta){
        if($karta%10 == 0 || $karta%10 == 1 || $karta%10 == 2 || $karta%10 == 3 || $karta%10 == 4)
            return 0;
        else if($karta%10 == 5)
            return 2;
        else if($karta%10 == 6)
            return 3;
        else if($karta%10 == 7)
            return 4;
        else if($karta%10 == 8)
            return 10;
        else
            return 11;
    }

    /**
        Funkcija dodjeljuje nove karte igracima na 2 mjesto u pripadnom polju karata
        Vraca novonastali redak
    */
    private function dodjeli_karte( $redak ){

        $polje_karata = explode(', ', $redak->karte);
        $spil = explode(', ', $redak->spil);
        $bacene = explode(', ', $redak->bacene_karte);
        for( $i = 0; $i < count( $polje_karata ); $i++ ){
            if( $polje_karata[$i] === $bacene[0] || $polje_karata[$i] === $bacene[1] ){
                if($redak->spil !== '')
                    $polje_karata[$i] = array_pop($spil);
                else
                    $polje_karata[$i] = '-1';
            }
        }
        $redak->karte = implode(', ', $polje_karata);
        $redak->spil = implode(', ', $spil);

        return $redak;

    }


    public function igra_u_tijeku_post(){

        $odgovor = Request::get('button');
        $prvi = DB::table('games')->where('email0', Auth::user()->email)->first();
        $drugi = DB::table('games')->where('email1', Auth::user()->email)->first();
        if( !empty($prvi))
            $redak = $prvi;
        else 
            $redak = $drugi;
        if( $redak->izasle === '')
            $redak->izasle = $odgovor;    
        else
            $redak->izasle = $redak->izasle . ', ' . $odgovor;
        if($redak->bacene_karte === '')
            $redak->bacene_karte = $odgovor;
        else
            $redak->bacene_karte = $redak->bacene_karte . ', ' . $odgovor;
        $redak->odigrali += 1;
        $redak->igrac_na_potezu = 1 - $redak->igrac_na_potezu;

        DB::table('games')->where('email', $redak->email)->update(array('izasle'=>$redak->izasle));
        DB::table('games')->where('email', $redak->email)->update(array('bacene_karte'=>$redak->bacene_karte));
        DB::table('games')->where('email', $redak->email)->update(array('odigrali'=> $redak->odigrali));
        DB::table('games')->where('email', $redak->email)->update(array('igrac_na_potezu'=> $redak->igrac_na_potezu));
        return redirect ('igraUTijeku');

    }

    private function provjeri_kraj_igre($redak){

        $karte = explode(', ', $redak->karte);
        for( $i = 0; $i < count($karte); $i++ )
            if($karte[$i] !== '-1')
                return 0;
        return 1;


    }

}
