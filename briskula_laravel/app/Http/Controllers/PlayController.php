<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Models\Online;
use Request;
use DB;

class PlayController extends Controller
{
	//funkcija vraća view svih igrača koji su trenutno online
    public function start(){
    	
    	//provjeravamo da li netko zeli igrati s nama
    	$tkoSaljeZahtjev = DB::table('onlines')->where('email', Auth::user()->email)->value('protivnik');
    	if( $tkoSaljeZahtjev === '' ){
    		$onlines = Online::all();
    		return view('play.online', compact( 'onlines' ) );
    	}
    	else{

    		return view('play.potvrda')->with('ja', Auth::user()->email)->with('protivnik', $tkoSaljeZahtjev);
    		//echo 'Hoces se igrati!?';
    	}

    }

    /*public function refresh(){

    	$onlines = Online::all();
    	return view('play.refreshOnline', compact('onlines'));

    }*/

    public function call(){

    	//otkrijemo tko je pozvao igraca u post buttonu i ododamo ga odabranom igracu
    	$tko = Auth::user()->email;
    	$koga = Request::get('button');
    	if(DB::table('onlines')->where('email', $koga)->value('protivnik') === ''){
    		DB::table('onlines')->where('email', $koga)->update(array('protivnik'=> $tko));
    		//return view('play.call')->with('tko', $tko)->with('koga', $koga);
    		return redirect('cekanje');
    	}
    	/*else if(DB::table('onlines')->where('email', $koga)->value('protivnik') === Auth::user()->email){
    		if(DB::table('onlines')->where('email', $koga)->value('potvrda') === 1){

    			echo "Start game pozivatelj";

    		}
    		else{

  			    return view('play.call')->with('tko', $tko)->with('koga', $koga);

    		}
    	
    	}*/
    	else{
    		return redirect('play');
   		}
    	//echo 'Bok';

    }

    public function cekanje(){

    	$koga = DB::table('onlines')->where('protivnik', Auth::user()->email)->value('email');
		if(DB::table('onlines')->where('email', $koga)->value('protivnik') === Auth::user()->email){
	    	if(DB::table('onlines')->where('email', $koga)->value('potvrda') === '1' ){

	    		return redirect('igra');

	    	}
	    	else{

	  		    return view('play.cekanje');
	  		    //echo  DB::table('onlines')->where('email', $koga)->value('potvrda');

	   		}
	   	}
	   	else 
	   		return redirect('play');

    }

    public function obradi(){

    	$odgovor = Request::get('button');
    	if( $odgovor === 'Da'){

    		//ako je odgovor da onda moramo otvoriti novu igru
    		DB::table('onlines')->where('email', Auth::user()->email)->update(array('potvrda'=> '1'));
	    	return redirect('igra');
    	}
    	else{
    		DB::table('onlines')->where('email', Auth::user()->email)->update(array('protivnik'=> ''));
    		return redirect('play');
  		}
    }
}
