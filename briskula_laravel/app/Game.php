<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
	/*protected $email0;
    protected $email1;
    protected $karte0;
    protected $karte1;
    protected $spil;
    protected $izasle;
    protected $briskula;
   	protected $bodovi0;
    protected $bodovi1;
    protected $igrac_na_potezu;
    protected $odigrali;
    protected $bacene_karte;

    public function __construct( $mail0, $mail1 ){
    	$this->email0 = $mail0;
    	$this->email1 = $mail1;
    	$this->spil = range( 0, 39 );
    	shuffle( $this->spil );
    	$this->karte0 = array();
    	for( $i = 0; $i < 3; $i++ )
    		$this->karte0[$i] = array_pop($this->spil);
    	$this->karte1 = array();
    	for( $i = 0; $i < 3; $i++ )
    		$this->karte1[$i] = array_pop($this->spil);
    	$this->briskula = array_pop($this->spil);
    	$this->izasle = array();
    	$this->bodovi0 = $this->bodovi1 = 0;
    	$this->igrac_na_potezu = 0;
    	$this->odigrali = 0;
    	$this->bacene_karte = array();
    	     }*/
   	protected $fillable = [
    	'email', 'karte', 'spil', 'izasle', 
    	'briskula', 'bodovi',  'igrac_na_potezu', 'odigrali', 'bacene_karte'
    ];

}
