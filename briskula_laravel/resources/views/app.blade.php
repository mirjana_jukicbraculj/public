<!DOCTYPE html>

<html lang='hr'>
	<head>

		<meta charset="UTF-8">
		<title>Document</title>
		<script>

			var interval = 1000;  // 1000 = 1 second, 3000 = 3 seconds
			function doAjax() {
   			 $.ajax({
          		type: 'GET',
          		action: 'ArticlesController@refresh',
           		data: $(this).serialize();
           		success: function (data) {
                $('#hidden').val(data);// first set the value     
           	 },
            	complete: function (data) {
                    // Schedule the next
                    setTimeout(doAjax, interval);
            }
    });
}
setTimeout(doAjax, interval);

		</script>
	</head>

	<body>

			<div id="hidden">
				@yield( 'content' )
			</div>

			@yield( 'footer' )

	</body>
</html>