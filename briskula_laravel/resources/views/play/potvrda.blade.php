@extends('layouts.play')

@section( 'content' )

	<h2> Prima zahtjev </h2>
	<p> {{ $protivnik }} calling {{ $ja }} .. </p>

	{!! Form::open(['url' => 'play']) !!}
		
			
		{!! Form::submit('Da', array('name' => 'button', 'value' => 'Da' ) ) !!} 
		{!! Form::submit('Ne', array('name' => 'button', 'value' => 'Ne' ) ) !!} 

	{!! Form::close() !!}

@endsection