@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <!--You are logged in! -->
                    Uspjesno ste ulogirani!
                </div>
                <div>
                    
                    Tu zelimo iscrtati korisnike koji su trenutno ulogirani!

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
